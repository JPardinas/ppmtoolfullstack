package com.jpardinas.ppmtool.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpardinas.ppmtool.domain.Project;
import com.jpardinas.ppmtool.exceptions.ProjectIdException;
import com.jpardinas.ppmtool.repositories.ProjectRepository;

@Service
public class ProjectService {
	
	@Autowired
	private ProjectRepository projectRepository;
	
	public Project saveOrUpdateProject (Project project) {
		
		try {
			project.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
			return projectRepository.save(project);
		} catch (Exception e) {
			throw new ProjectIdException("Project ID '"+project.getProjectIdentifier().toUpperCase()+"' already exist.");
		}
	}
	
	public Project findByProjectIdentifier (String projectId) {
		
		Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());
		
		if (project == null) {
			throw new ProjectIdException("Project ID '"+projectId+"' does not exist.");
		}
		
		return project;
	}
	
	public Iterable<Project> findAllProjects () {
		return projectRepository.findAll();
	}
	
	public void deleteProjectByIdentifier (String projectId) {
		
		Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());
		
		if (project == null) {
			throw new ProjectIdException("Can not delete Project with ID '"+projectId+"'. This project does not exist.");
		}
		
		projectRepository.delete(project);
	}

}
